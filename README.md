# examples

Various examples.

## POSIX message queues

 - mq_send -- send and receive messages over *mq*.
 - mq_notify -- register messages notification.
