#pragma once

struct qmsg {
	int id;
	size_t length;
	char content[255];
};
