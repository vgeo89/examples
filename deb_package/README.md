# deb-пакет

Пример организации сборки утилит в deb-пакет

# Сборка

Для сборки только утилит выполнить `make`.

Для сборки всего пакета выполнить: `dpkg-buildpackage -B` либо `make deb`.

Для очистки сборки выполнить `make clean && make mrproper`.

# Примеры

`./hello`

# Авторы

Vladimir Georgiev <v.georgiev@metrotek.ru>

# Лицензия

MIT
