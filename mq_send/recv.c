#include <stdio.h>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

#include "msg.h"

int main(int argc, char **argv)
{
	struct qmsg msg;

	struct mq_attr attr = {
		.mq_maxmsg = 1,
		.mq_msgsize = sizeof(struct qmsg),
	};

	if (argc > 1) {
		mq_unlink("/q1");
	}

	mqd_t q = mq_open("/q1", O_RDONLY | O_CREAT | O_EXCL, S_IRWXU, &attr);

	if (q == -1) {
		fprintf(stderr, "Unable to create queue: %s\n", strerror(errno));
		return errno;
	}

	if (mq_receive(q, (char*)&msg, sizeof(struct qmsg), 0) == -1) {
		fprintf(stderr, "Unable to receive a message: %s\n", strerror(errno));
	}
	else {
		printf("Received message ID %d, length=%lu\n", msg.id, msg.length);
		for (int i = 0; i < msg.length; ++i) {
			printf("%c", msg.content[i]);
		}
		printf("\n");
	}

	if (mq_close(q) == -1) {
		fprintf(stderr, "Close error: %s\n", strerror(errno));
	}

	if (mq_unlink("/q1") == -1) {
		fprintf(stderr, "Unlink error: %s\n", strerror(errno));
	}

	return 0;
}
