#include <stdio.h>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

#include "msg.h"

int main(int argc, char **argv)
{
	struct qmsg msg = {
		.id = 100,
		.length = 6,
		.content = {'h', 'e', 'l', 'l', 'o', '\0'},
	};

	mqd_t q = mq_open("/q1", O_WRONLY);

	if (q == -1) {
		fprintf(stderr, "Unable to create queue: %s\n", strerror(errno));
		return errno;
	}

	if (mq_send(q, (const char*)&msg, sizeof(struct qmsg), 0) == -1) {
		fprintf(stderr, "Unable to send a message: %s\n", strerror(errno));
	}

	if (mq_close(q) == -1) {
		fprintf(stderr, "Close error: %s\n", strerror(errno));
	}

	return 0;
}
