#include <stdio.h>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>

#include "msg.h"

struct message_callback_args {
	struct sigevent signal_event;
	mqd_t message_queue;
};

/* There is no way to pass arguments into a signal handler. So use global flag. */
static volatile bool keep_going = true;

static void sigint_calback(int signum)
{
	keep_going = false;
}

static void message_callback(union sigval sv)
{
	struct qmsg msg;
	struct message_callback_args args =
			*((struct message_callback_args*) sv.sival_ptr);

	if (mq_receive(args.message_queue, (char *)&msg,
					sizeof(struct qmsg), 0) == -1) {
		fprintf(stderr, "Unable to receive a message: %s\n", strerror(errno));
	}
	else {
		printf("Received message ID %d, length=%lu\n", msg.id, msg.length);
		for (int i = 0; i < msg.length; ++i) {
			printf("%c", msg.content[i]);
		}
	}

	/* Re-register a notification */
	mq_notify(args.message_queue, &args.signal_event);
}

int main(int argc, char **argv)
{
	struct mq_attr attr = {
		.mq_maxmsg = 1,
		.mq_msgsize = sizeof(struct qmsg),
	};

	struct message_callback_args cb_args;

	/* Pass something as an program ergument to unlink previous queue */
	if (argc > 1) {
		mq_unlink("/q1");
	}

	cb_args.message_queue = mq_open("/q1", O_RDONLY | O_CREAT | O_EXCL,
								S_IRWXU, &attr);

	if (cb_args.message_queue == -1) {
		fprintf(stderr, "Unable to create queue: %s\n", strerror(errno));
		return errno;
	}

	cb_args.signal_event.sigev_notify = SIGEV_THREAD;
	cb_args.signal_event.sigev_notify_function = message_callback;
	cb_args.signal_event.sigev_notify_attributes = NULL;
	cb_args.signal_event.sigev_value.sival_ptr = &cb_args;

	if (mq_notify(cb_args.message_queue, &cb_args.signal_event) == -1) {
		fprintf(stderr, "Unable to create queue: %s\n", strerror(errno));
		goto exit_app;
	}

	signal(SIGINT, sigint_calback);

	while(keep_going) {
		usleep(1000);
	}

exit_app:

	if (mq_close(cb_args.message_queue) == -1) {
		fprintf(stderr, "Close error: %s\n", strerror(errno));
	}

	if (mq_unlink("/q1") == -1) {
		fprintf(stderr, "Unlink error: %s\n", strerror(errno));
	}

	return 0;
}
